<img src="https://pbs.twimg.com/media/FYTV3tsakAE_J1K?format=jpg" width="100%">

Greetings, adventurer! Welcome to Delman's DevOps quest. I’m your kinsman, and I need your help to help us design a stable yet performant infrastructure to help our users manage data seamlessly. Before you start your very adventure, here is some equipment you should carry on:

- Terraform
- Kubernetes (we’ll use k8d here for ease of use)
- GitLab accounts

The idea is that we will run the application on a Kubernetes cluster with the basic tools we’ve mentioned before. One of our components is a web server that handles user requests so that our other core components can initiate the entire process pipeline as intended.

Our application will be memory-heavy than a CPU-heavy one. Memory usages increase linearly aligned with sizes of the data being processed. Meaning that you have to think of a strategy to avoid the application being OOM-ed due to signal 9 attacks.

In this quest, we have an nginx application running on any OCI-compatible container runtime, which we use containerd. If you’re using Docker, you most likely already use containerd by the way.

The diagrams are very boring. Just think like your user will hit ingress which will be forwarded to the appropriate service based on its hostname, and then eventually hit our very nginx application. Sounds easy, no?

The challenge is that we’re going to do some kind of "chaos testing" which will make the application take up as much memory as intended as we try our best to give you as real a problem as possible. We currently have no challenges with some kind of CI/CD things, but you have to be prepared with your best answer anyway!

**Main quests:**

Propose your scaling strategy using HPA or VPA or both. Showing us the math is a big plus

**Point plus, if you:**

- Implement some kind of boundary such as ResourceQuota and/or LimitRange
- Write Kubernetes resources in Terraform than a plain YAML
- Already familiar with Helm (big plus)
- Already familiar with Keda (big plus)
- Using NixOS

**Objectives:**

- Can scale out the workload when application consumes 69% of requested memory
- Can scale out the workload when application consumes 120% of requested memory
- Can scale down the workload when peak ends
- Keep the "restart (due to pod crash)" number as low as possible
- And the most important: maintain as much as possible 200 status codes regardless of upstream states

**Getting started:**

- Make sure you have a kubernetes cluster set up on your machine
- Make sure port 31337 is reserved on your machine
- Clone the `kusir-ops/devops-tht` repository from GitLab
- Run `scripts/preflight.sh` OR if you know what that `shell.nix` thing is, it must be easier for both of us ;)
- Run `scripts/run.sh` and wait until finish
- If things goes well, you can check the application via port 31337 (i.e: localhost:31337) on your browser, just to be extra sure
- Eventually messing around the application! You can also use Chaos Mesh and/or wrk2 on your end like we did!
- Push your proposed changes to your own repository

If you have any questions my email is fariz@delman.io. You are more than welcome to hit me up just to say hi or send some feedback.

And the most important is: **Good luck, have fun!**

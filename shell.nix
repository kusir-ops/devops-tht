with import <nixpkgs> {};

pkgs.mkShell {
  name = "devops-tht";

  buildInputs = [
    terraform
    kubectl
    kubectx
    kubernetes-helm
  ];
}
